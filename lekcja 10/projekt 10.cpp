#include <iostream>

/*run this program using the console pauser or add your own geth, system("pause")*/
using namespace std;

int main(int argc, char** arg) {
	    int n;
	    int wynik;
	    
	    cout<<"Podaj rozmiar macierzy:\n";
	    cin>>n;
	    cout<<endl;
	    
	for(int i=1;i<=n;i++) {
		for(int j=1;j<=n;j++){
			wynik = i*j;
			cout<<""<<i<<"*"<<j<<"="<<wynik;
	    	cout<<endl;
    }
        cout<<endl;
	}
	
	return 0;
}
