#include <iostream>
#include <string>
#include <cstdlib>
#include <algorithm>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
using namespace std;


int main(int argc, char** argv) {
	int zakres;
	cout<<"Podaj liczbe zakresow liczb do posortowania\n";
	cin>>zakres;
	
	
	cout<<"podaj do posortowania 5 liczb \n";
	int liczby[zakres];
	
	for(int i=0;i<zakres;i++) cin>>liczby[i];
	
	sort(liczby, liczby+zakres);
	
	cout<<"elementy posortowane:"<<endl;
	
	for(int i=0;i<zakres;i++) {
		cout<<liczby[i]<<" ";
	}
	cout<<endl;
	return 0;
}
